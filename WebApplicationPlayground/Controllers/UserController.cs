﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebApplicationPlayground.Models;

namespace WebApplicationPlayground.Controllers
{
    [Route("api/user")]
    public class UserController : Controller
    {
        private readonly Context _context;

        public UserController(Context context)
        {
            _context = context;
        }

        [HttpPut("{id:guid}/name")]
        public async Task<IActionResult> UpdateName(Guid id, [FromBody] UserDto request)
        {
            var user = _context.Users.Where(b => b.Id == id).FirstOrDefault();
            if (user == null)
            {
                return NotFound();
            }

            user.Name = request.Name;

            int result = await _context.SaveChangesAsync();

            return Ok(result);
        }
        
        [HttpPut("{id:guid}/address")]
        public async Task<IActionResult> AddAdderess(Guid id, string address)
        {
            var user = _context.Users
                .Where(b => b.Id == id)
                .Include(i => i.Addresses)
                .FirstOrDefault();
            
            if (user == null)
            {
                return NotFound();
            }

            user.AddAddress(address);

            int result = await _context.SaveChangesAsync();

            return Ok(result);
        }

        [HttpPut("{id:guid}")]
        public async Task<IActionResult> Update(Guid id, [FromBody] UserDto request)
        {
            var user = _context.Users.Where(b => b.Id == id).FirstOrDefault();
            if (user == null)
            {
                return NotFound();
            }

            user.Name = request.Name;
            user.State = request.State;

            await _context.SaveChangesAsync();

            return Ok();
        }

        [HttpPost]
        public IActionResult Create([FromBody] UserDto request)
        {
            User user = new User(request.Name, 0, request.Addresses);

            _context.Add(user);

            _context.SaveChanges();

            return Ok();
        }

        [HttpGet("{id:guid}")]
        public IActionResult Get(Guid id)
        {

            var user = _context.Find<User>(id);
            if (user == null)
            {
                return NotFound();
            }

            var userDto = new UserDto()
            {
                Name = user.Name,
                State = user.State
            };

            return Ok(userDto);
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var users = _context.Users.ToList();

            var userDtos = users.Select(user => new UserDto()
            {
                Id = user.Id,
                Name = user.Name,
                State = user.State
            }).ToList();

            return Ok(userDtos);
        }
    }

    public class UserDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int State { get; set; }

        public List<Address> Addresses { get; set; }
    }
}
