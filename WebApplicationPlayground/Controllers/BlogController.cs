﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationPlayground.Models;

namespace WebApplicationPlayground.Controllers
{
    [Route("api/blog")]
    [ApiController]
    public class BlogController : ControllerBase
    {
        private readonly Context _context;

        public BlogController(Context context)
        {
            _context = context;
        }

        [HttpPost("test")]
        public async Task<IActionResult> CreateTest([FromBody] BlogDto request)
        {

            for (int i = 0; i < 1000000; i++)
            {
                Blog blog = new Blog()
                {
                    Url = request.Url,
                };

                _context.Add(blog);
            }

            await _context.SaveChangesAsync();

            return Ok();
        }

        [HttpPost]
        public IActionResult Create([FromBody] BlogDto request)
        {
            Blog blog = new Blog()
            {
                Url = request.Url,
            };

            _context.Add(blog);

            _context.SaveChanges();

            return Ok();
        }

        [HttpPut("{id:guid}")]
        public async Task<IActionResult> Update(Guid id, [FromBody] BlogDto request)
        {
            var blog = _context.Blogs.Where(b => b.Id == id).FirstOrDefault();
            if (blog == null)
            {
                return NotFound();
            }

            blog.Url = request.Url;

            await _context.SaveChangesAsync();

            return Ok();
        }

        [HttpGet("{id:guid}")]
        public IActionResult Get(Guid id)
        {

            var blog = _context.Find<Blog>(id);
            if (blog == null)
            {
                return NotFound();
            }

            var blogDto = new BlogDto()
            {
                Url = blog.Url,
                Code = blog.Code,
            };

            return Ok(blogDto);
        }

        [HttpGet("{code}")]
        public IActionResult GetByCode(string code)
        {
            var blog = _context.Blogs.Where(b => b.Code == code).FirstOrDefault();
            if (blog == null)
            {
                return NotFound();
            }

            var blogDto = new BlogDto()
            {
                Url = blog.Url,
                Code = blog.Code,
            };

            return Ok(blogDto);
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var blogs = _context.Blogs.ToList();

            var blogDtos = blogs.Select(blog => new BlogDto()
            {
                Id = blog.Id,
                Url = blog.Url,
                Code = blog.Code,
            }).ToList();

            return Ok(blogDtos);
        }
    }

    public class BlogDto
    {
        public Guid Id { get; set; }
        public string Url { get; set; }
        public string Code { get; set; }
    }

}
