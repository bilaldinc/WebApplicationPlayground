﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationPlayground.Models;

namespace WebApplicationPlayground.Controllers
{
    [Route("api/item")]
    public class ItemController : ControllerBase
    {
        private readonly Context _context;

        public ItemController(Context context)
        {
            _context = context;
        }

        [HttpPut("{id:guid}")]
        public async Task<IActionResult> Update(Guid id, [FromBody] ItemDto request)
        {
            var item = _context.Items.Where(b => b.Id == id).FirstOrDefault();
            if (item == null)
            {
                return NotFound();
            }

            item.Name = request.Name;
            item.OtherName = request.OtherName;

            await _context.SaveChangesAsync();

            return Ok();
        }

        [HttpPost]
        public IActionResult Create([FromBody] ItemDto request)
        {
            Item item = new Item()
            {
                Name = request.Name,
                OtherName = request.OtherName,
            };

            _context.Add(item);

            _context.SaveChanges();

            return Ok();
        }

        [HttpGet("{id:guid}")]
        public IActionResult Get(Guid id)
        {

            var item = _context.Find<Item>(id);
            if (item == null)
            {
                return NotFound();
            }

            var itemDto = new ItemDto()
            {
                Name = item.Name,
                OtherName = item.OtherName,
                Version = item.Version
            };

            return Ok(itemDto);
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var items = _context.Items.ToList();

            var itemDtos = items.Select(item => new ItemDto()
            {
                Id = item.Id,
                Name = item.Name,
                OtherName = item.OtherName,
                Version = item.Version
            }).ToList();

            return Ok(itemDtos);
        }
    }
    public class ItemDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string OtherName { get; set; }
        public uint Version { get; set; }
    }
}
