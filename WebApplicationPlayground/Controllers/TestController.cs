﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.Json;
using Microsoft.AspNetCore.Mvc;

namespace WebApplicationPlayground.Controllers;

[Route("api/test")]
[ApiController]
public class TestController : ControllerBase
{
    [HttpPut("")]
    public IActionResult Test(JsonDocument jsonDocument)
    {
        JsonElement root = jsonDocument.RootElement;
        JsonElement temp;

        var request = new Requst();
        foreach (PropertyInfo property in typeof(Requst).GetProperties())
        {
            if (property.PropertyType.IsGenericType &&
                property.PropertyType.GetGenericTypeDefinition() == typeof(ValueWraper<>))
            {
                bool existt = root.TryGetProperty(ToCamel(property.Name), out temp);
                if (existt)
                {
                    Type asdfff = property.PropertyType.GenericTypeArguments.First();
                    temp.Deserialize(asdfff);
                    // temp.Deserialize(asdfff)
                }
            }
            string asdff = property.Name;
        }
        // var request = new Requst();
        /*bool exist = root.TryGetProperty("categoryBrandModelId", out temp);
        if (exist)
        {
            request.CategoryBrandModelId = new ValueWraper<Guid>() { IsUpdated = true, Value = temp.GetGuid() };
        }*/
        
        return Ok();
    }

    public string ToCamel(string s)
    {
        return char.ToLower(s[0]).ToString() + s.Substring(1);
    }
    
    [HttpPut("/2")]
    public IActionResult Test2(JsonDocument jsonDocument)
    {
        JsonElement root = jsonDocument.RootElement;
        
        var options = new JsonSerializerOptions
        {
            PropertyNameCaseInsensitive = true
        };
        
        // root.GetProperty("categoryBrandModelId").Deserialize<ValueWraper<Guid>>(options)
        // root.GetProperty("categoryBrandModelId").GetProperty("isUpdated").Deserialize<bool>()

        var req = new Requst
        {
            AssetIds = root.GetProperty("assetIds").Deserialize<List<Guid>>(options),
            CategoryBrandModelId = root.GetProperty("categoryBrandModelId").Deserialize<ValueWraper<Guid>>(options),
            LocationId = root.GetProperty("locationId").Deserialize<ValueWraper<Guid?>>(options),
            CustodyUserId = root.GetProperty("custodyUserId").Deserialize<ValueWraper<Guid?>>(options),
            SerialNo = root.GetProperty("serialNo").Deserialize<ValueWraper<string>>(options),
            PurchasedFirm = root.GetProperty("purchasedFirm").Deserialize<ValueWraper<string>>(options),
            PurchasedDate = root.GetProperty("purchasedDate").Deserialize<ValueWraper<DateTimeOffset?>>(options),
            PurchasedCost = root.GetProperty("purchasedCost").Deserialize<ValueWraper<decimal?>>(options),
            ProductionDate = root.GetProperty("productionDate").Deserialize<ValueWraper<DateTimeOffset?>>(options),
            Notes = root.GetProperty("notes").Deserialize<ValueWraper<string>>(options),
        };
        return Ok();
    }
}

public class Requst
{
    public List<Guid> AssetIds { get; set; }
    public ValueWraper<Guid> CategoryBrandModelId { get; set; }
    public ValueWraper<Guid?> LocationId { get; set; }
    public ValueWraper<Guid?> CustodyUserId { get; set; }
    public ValueWraper<string> SerialNo { get; set; }
    public ValueWraper<string> PurchasedFirm { get; set; }
    public ValueWraper<DateTimeOffset?> PurchasedDate { get; set; }
    public ValueWraper<decimal?> PurchasedCost { get; set; }
    public ValueWraper<DateTimeOffset?> ProductionDate { get; set; }
    public ValueWraper<string> Notes { get; set; }
}
public class ValueWraper<T>
{
    public T Value { get; set; }
    public bool IsUpdated { get; set; }
}
