﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebApplicationPlayground.Models;

namespace WebApplicationPlayground.Controllers
{
    [Route("api/ticket")]
    public class TicketController : ControllerBase
    {
        private readonly Context _context;

        public TicketController(Context context)
        {
            _context = context;
        }

        [HttpPut("{id:guid}")]
        public async Task<IActionResult> Update(Guid id, [FromBody] TicketDto request)
        {
            var ticket = _context.Tickets.Where(b => b.Id == id).FirstOrDefault();
            if (ticket == null)
            {
                return NotFound();
            }

            ticket.Title = request.Title;

            await _context.SaveChangesAsync();

            return Ok();
        }

        [HttpPost]
        public IActionResult Create([FromBody] TicketDto request)
        {
            Ticket ticket = new Ticket()
            {
                Title = request.Title,
            };

            _context.Add(ticket);

            _context.SaveChanges();

            return Ok();
        }

        [HttpGet("{id:guid}")]
        public IActionResult Get(Guid id)
        {

            var ticket = _context.Find<Ticket>(id);
            if (ticket == null)
            {
                return NotFound();
            }

            var ticketDto = new TicketDto()
            {
                Title = ticket.Title,
                Version = ticket.Version
            };

            return Ok(ticketDto);
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var tickets = _context.Tickets.ToList();

            var ticketDtos = tickets.Select(ticket => new TicketDto()
            {
                Id = ticket.Id,
                Title = ticket.Title,
                Version = ticket.Version
            }).ToList();

            return Ok(ticketDtos);
        }
        
        [HttpPut("{id:guid}/note")]
        public async Task<IActionResult> AddNote(Guid id, string note)
        {
            var ticket = _context.Tickets
                .Where(b => b.Id == id)
                .Include(i => i.TicketNotes)
                .FirstOrDefault();
            
            if (ticket == null)
            {
                return NotFound();
            }

            ticket.AddNote(note);

            int result = await _context.SaveChangesAsync();

            return Ok(result);
        }
        
        [HttpDelete("{id:guid}/note/{noteId:guid}")]
        public async Task<IActionResult> AddNote(Guid id, Guid noteId, string note)
        {
            var ticket = _context.Tickets
                .Where(b => b.Id == id)
                .Include(i => i.TicketNotes)
                .FirstOrDefault();
            
            if (ticket == null)
            {
                return NotFound();
            }

            ticket.RemoveNote(noteId);

            int result = await _context.SaveChangesAsync();

            return Ok(result);
        }
        
        [HttpPut("{id:guid}/note/{noteId:guid}")]
        public async Task<IActionResult> UpdateNote(Guid id, Guid noteId, string note)
        {
            var ticket = _context.Tickets
                .Where(b => b.Id == id)
                .Include(i => i.TicketNotes)
                .FirstOrDefault();
            
            if (ticket == null)
            {
                return NotFound();
            }

            ticket.ChangeNote(noteId, note);

            int result = await _context.SaveChangesAsync();

            return Ok(result);
        }
    }

    public class TicketDto
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public Guid Version { get; set; }
    }
}
