﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using WebApplicationPlayground.Models;

namespace WebApplicationPlayground.Controllers
{
    [Route("api/location")]
    public class LocationController : Controller
    {
        private readonly Context _context;

        public LocationController(Context context)
        {
            _context = context;
        }

        [HttpPost]
        public IActionResult Create([FromBody] LocationDto request)
        {
            Location location = new Location()
            {
                Name = request.Name,
                ParentId = request.ParentId,
            };

            _context.Add(location);

            _context.SaveChanges();

            return Ok();
        }

        [HttpGet("{id:guid}")]
        public IActionResult Get(Guid id)
        {

            var location = _context.Locations
                .Include(x => x.Childerens)
                .Where(x => x.Id == id)
                .FirstOrDefault();

            if (location == null)
            {
                return NotFound();
            }

            return Ok(location);
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            return Ok(GetLocationTree_ExplicitLoading());
        }

        private List<LocationTreeDto> GetLocationTree_LoadingAllTable()
        {
            // This works too
            // var locations = _context.Locations.Include(x => x.Childerens).ToList().Where(x => x.ParentId == null).ToList();

            // This doesn't work in EF Core
            // var locations = _context.Locations.Include(x => x.Childerens).Where(x => x.ParentId == null).ToList();

            var locations = _context.Locations.AsEnumerable().Where(x => x.ParentId == null).ToList();

            return locations.Select(i => new LocationTreeDto(i)).ToList();
        }

        private List<LocationTreeDto> GetLocationTree_ExplicitLoading()
        {
            var locations = _context.Locations.Where(x => x.ParentId == null).ToList();
            foreach (var location in locations)
            {
                LoadAllChilderen(location);
            }

            return locations.Select(i => new LocationTreeDto(i)).ToList();
        }

        private void LoadAllChilderen(Location location)
        {
            _context.Entry(location).Collection(i => i.Childerens).Load();

            foreach (var child in location.Childerens)
            {
                LoadAllChilderen(child);
            }
        }
    }


    public class LocationDto
    {
        public string Name { get; set; }
        public Guid? ParentId { get; set; }
    }
    public class LocationTreeDto
    {
        public LocationTreeDto(Location location)
        {
            Name = location.Name;
            if (location.Childerens is not null)
            {
                Childerens = location.Childerens.Select(i => new LocationTreeDto(i)).ToList();
            }
            else
            {
                Childerens = null;
            }
        }

        public string Name { get; set; }
        public List<LocationTreeDto> Childerens { get; set; }
    }

}
