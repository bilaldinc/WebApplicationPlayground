﻿using System;

namespace WebApplicationPlayground
{
    public interface IVersionedEntity
    {
        Guid Version { get; set; }
    }
}
