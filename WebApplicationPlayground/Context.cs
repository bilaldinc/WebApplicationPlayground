﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Metadata;
using WebApplicationPlayground.EntityTypeConfigurations;
using WebApplicationPlayground.Models;

namespace WebApplicationPlayground
{
    public class Context : DbContext
    {
        public DbSet<Blog> Blogs { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<Ticket> Tickets { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Location> Locations { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.LogTo(Console.WriteLine, LogLevel.Information);
            optionsBuilder
                .UseNpgsql("Host=localhost;Port=5432;Database=mydb;Username=admin;Password=borda");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(Context).Assembly);
        }

        public override int SaveChanges()
        {
            VersionedEntityHandler();
            
            // base.OnModelCreating();
            return base.SaveChanges();
        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            VersionedEntityHandler();

            return await base.SaveChangesAsync(true, cancellationToken);
        }

        private void VersionedEntityHandler()
        {
            foreach (var changedEntity in ChangeTracker.Entries())
            {
                if (changedEntity.Entity is IVersionedEntity entity)
                {
                    foreach (CollectionEntry nav in changedEntity.Collections)
                    {
                        
                    }
                    bool asd0 = changedEntity.Collections.Any(i => i.IsModified);
                    bool asd1 = changedEntity.Navigations.Any(i => i.IsModified);
                    bool asd2 = changedEntity.Navigations.Any(i => i.EntityEntry.State is (EntityState.Added or EntityState.Modified));
                    if (changedEntity.State is (EntityState.Added or EntityState.Modified))
                    {
                        entity.Version = Guid.NewGuid();
                    }
                }
            }
        }

    }
}
