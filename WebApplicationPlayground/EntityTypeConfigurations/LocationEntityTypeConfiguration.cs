﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WebApplicationPlayground.Models;

namespace WebApplicationPlayground.EntityTypeConfigurations
{
    public class LocationEntityTypeConfiguration : IEntityTypeConfiguration<Location>
    {
        public void Configure(EntityTypeBuilder<Location> builder)
        {

            builder
                .Property(b => b.Name)
                .IsRequired();

        }
    }
}
