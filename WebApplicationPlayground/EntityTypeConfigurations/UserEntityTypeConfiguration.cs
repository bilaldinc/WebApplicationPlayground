﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WebApplicationPlayground.Models;

namespace WebApplicationPlayground.EntityTypeConfigurations
{
    public class UserEntityTypeConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder
                .Property(b => b.Name)
                .IsRequired();

            builder
                .Property(b => b.State)
                .IsConcurrencyToken()
                .IsRequired();

            builder.OwnsMany(u => u.Addresses);
        }
    }
}
