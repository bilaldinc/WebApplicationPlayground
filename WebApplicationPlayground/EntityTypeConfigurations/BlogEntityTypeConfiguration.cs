﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WebApplicationPlayground.Models;

namespace WebApplicationPlayground.EntityTypeConfigurations
{
    public class BlogEntityTypeConfiguration : IEntityTypeConfiguration<Blog>
    {
        public void Configure(EntityTypeBuilder<Blog> builder)
        {
            builder
                .UseXminAsConcurrencyToken();

            builder
                .Property(b => b.Url)
                .IsRequired();

            builder
                .Property(b => b.Code)
                .HasConversion(v => long.Parse(v.Replace("LH-", "")), v => "LH-" + v.ToString())
                .UseIdentityAlwaysColumn()
                .HasIdentityOptions(startValue: 1000, incrementBy: 1);

            builder
                .HasIndex(b => b.Code)
                .IsUnique();
        }
    }

}
