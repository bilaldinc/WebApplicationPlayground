﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WebApplicationPlayground.Models;

namespace WebApplicationPlayground.EntityTypeConfigurations
{
    public class TicketEntityTypeConfiguration : IEntityTypeConfiguration<Ticket>
    {
        public void Configure(EntityTypeBuilder<Ticket> builder)
        {
            builder
                .Property(b => b.Title)
                .IsRequired();

            builder
                .Property(b => b.Version)
                .IsConcurrencyToken()
                .IsRequired();
            
            builder.OwnsMany(u => u.TicketNotes);
        }
    }
}
