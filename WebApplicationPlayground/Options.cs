﻿namespace WebApplicationPlayground
{
    public class Options
    {
        public const string SectionName = "FileService";

        public string BucketName { get; set; }
        public string Endpoint { get; set; }
        public int Port { get; set; }
        public string AccessKey { get; set; }
        public string SecretKey { get; set; }
    }
}