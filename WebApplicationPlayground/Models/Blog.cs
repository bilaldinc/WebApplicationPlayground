﻿using System;

namespace WebApplicationPlayground.Models
{
    public class Blog
    {
        public Guid Id { get; set; }

        public string Code { get; }

        public string Url { get; set; }

    }
}
