﻿using System;

namespace WebApplicationPlayground.Models
{
    public class Item
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string OtherName { get; set; }
        public uint Version { get; set; }

    }
}
