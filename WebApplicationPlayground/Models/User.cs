﻿using System;
using System.Collections.Generic;

namespace WebApplicationPlayground.Models
{
    public class User
    {
        public User()
        {
            _adresses = new List<Address>();
        }

        public User(string name, int state, List<Address> adresses)
        {
            Name = name;
            State = state;
            _adresses = adresses;
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public int State { get; set; }

        private readonly List<Address> _adresses;
        public IReadOnlyList<Address> Addresses => _adresses.AsReadOnly();

        public void AddAddress(string address)
        {
            _adresses.Add(new() { Name = address});
        }
    }
}
