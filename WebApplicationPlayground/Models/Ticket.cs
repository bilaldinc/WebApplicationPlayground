﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WebApplicationPlayground.Models
{
    public class Ticket : IVersionedEntity
    {
        private readonly List<TicketNotes> _ticketNotes = new();
        public IReadOnlyList<TicketNotes> TicketNotes => _ticketNotes.AsReadOnly();
        
        public Guid Id { get; set; }
        public string Title { get; set; }
        public Guid Version { get; set; }
        
        public void AddNote(string note)
        {
            _ticketNotes.Add(new() { Value = note});
        }
        
        public void RemoveNote(Guid id)
        {
            _ticketNotes.RemoveAll(i => i.Id == id);
        }
        
        public void ChangeNote(Guid id, string note)
        {
            var ticketNote = _ticketNotes.First(i => i.Id == id);

            ticketNote.Value = note;
        }
    }
}
