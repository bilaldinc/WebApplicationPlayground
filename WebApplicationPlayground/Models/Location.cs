﻿using System;
using System.Collections.Generic;

namespace WebApplicationPlayground.Models
{
    public class Location
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public Guid? ParentId { get; set; }

        public Location Parent { get; set; }

        public List<Location> Childerens { get; set; }
    }
}
