﻿using System;

namespace WebApplicationPlayground.Models;

public class TicketNotes
{
    public Guid Id { get; set; }
    public string Value { get; set; }
}